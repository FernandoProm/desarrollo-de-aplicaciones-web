<!DOCTYPE html>
<html>
   
    <head>
        <meta content="text/html; charset=UTF-8" http-equiv="content-type">
        <link rel="stylesheet" type= "text/css" href="Lab7.css" />
        <h1>
            <br>
            <br>
            Laboratorio #7<br>
        </h1>
    </head>
    
    <body>
        <?php //arreglos
            $array1 = array(4,5,9,10,7,7,9,1,2,6,8,9,0,0,1,5,3,1);
            $array2 = array(5,4,3,0,2,4,5,7,10,3,1,2,2,7,8,6,5,6,6,3,2,2,10,4,6,7,10);
        
            
            lista($array1, 1);
            lista($array2, 2);
            
            echo '<form method="POST" action="Lab7.php">
                        Dame numero entero positivo: 
                        <input type="text" name="num">
                        <input type="submit" name ="submit" value="send">
                 </form><br>';
                 
            if ($_POST["submit"] == "send") {
                  cubos($_POST["num"]);
            } 
            
            echo '<form method="POST" action="Lab7.php">
                        Dame numero entero positivo a invertir: 
                        <input type="text" name="num2">
                        <input type="submit" name ="submit2" value="send">
                 </form><br>';
                 
            if ($_POST["submit2"] == "send") {
                  invierte($_POST["num2"]);
            } 
            
        ?>
        
        <?php //imprime arreglo
            function display($array) {
                echo "[";
                for ($i = 0; $i < sizeof($array); $i++) {
                    if ($i < sizeof($array) - 1) {
                        echo "$array[$i], ";
                    } else {
                        echo "$array[$i]]<br>";
                    }
                }
            }
        ?>
        
        <?php //promedio
            function promedio($array) {
                $x = 0;
                
                for ($i = 0; $i < sizeof($array); $i++) {
                    $x += $array[$i];
                }
                $x = $x/sizeof($array);
                return $x;
            }
        ?>
        
        <?php //mediana
            function mediana($array) {
                $middle;
                $median;
                sort($array);

                $middle = sizeof($array)/2;
                $median = $array[$middle];
                if (sizeof($array) % 2 == 0) {
                    $median = ($median + $array[$middle - 1]) / 2;
                }
                return $median;
            }
        ?>
        
        <?php //mayor a menor.
            function lista($array, $num) {
                $lowToHigh = $highToLow = $array;
                sort($lowToHigh);
                rsort($highToLow);
                
                echo "<br><ul>
                            <li>Arreglo $num:
                                <ul>
                                    <li>";
                                        display($array);
                                    echo "</li>
                                </ul>
                            </li>
                            <li>Promedio:
                                <ul>
                                    <li>" . promedio($array) . "</li>
                                </ul>
                            </li>
                            <li>Media:
                                <ul>
                                    <li>" . mediana($array) . "</li>
                                </ul>
                            </li>
                            <li>Ordenado menor a mayor:
                                <ul>
                                    <li>";
                                        display($lowToHigh);
                              echo "</li>
                                </ul>
                            </li>
                            <li>Ordenado mayor a menor:
                                <ul>
                                    <li>";
                                        display($highToLow);
                              echo "</li>
                                </ul>
                            </li>
                        </ul><br><br>";
            }
        ?>
        
        <?php //cuadrados y cubos de 1 hasta n
            function cubos($num) {
                $temp;

                echo "<table><tr><th>Enteros</th><th>Cuadrados</th><th>Cubos</th></tr>";

                for ($i = 0; $i < $num; $i++) {
                    $temp=$i+1;
                    echo "<tr><td>" . pow($temp,1) . "</td>";
                    echo "<td>"     . pow($temp,2) . "</td>";
                    echo "<td>"     . pow($temp,3) . "</td></tr>";
                }
                echo "</table><br>";
            }
        ?>
        
        <?php //numeros invertidos
            function invierte($num) {
                
                echo "Normal: " . $num . "<br>";
                echo "Invertido: ";
                while($num != 0){
                    echo $num % 10;
                    $num = ( $num - $num % 10 ) / 10;
                }
                
                echo "<br><br>";
            }
        ?>
        
        <h1>Preguntas</h1>
        <h>¿Qué hace la función phpinfo()? Describe y discute 3 datos que llamen tu atención.</h>
        <p>Al llamar esta funcion, la interfaz muestra toda la información sobre la configuración de PHP.
Como por ejemplo: INFO_GENERAL: la linea de configuración. INFO_CREDITS: creditos de PHP. INFO_CONFIGURATION: valores Local y Master para directivas PHP.</p>
        
        <h>¿Qué cambios tendrías que hacer en la configuración del servidor para que pudiera ser apto en un ambiente de producción?</h>
        <p>
		para poder procesar un archivo PHP en tu servidor es necesario instalarle el software (por ejemplo Apache) para que pueda entender y traducir todo el documento e interacciones con el usuario.
		
		</p>
        <h>¿Cómo es que si el código está en un archivo con código html que se despliega del lado del cliente, se ejecuta del lado del servidor? Explica.</h>
        <p>
		
		El servidor procesa el archivo PHP y la respuesta la traduce a un archivo HTML que es el que se envia al cliente para que lo observe.
			
		</p>
        
    </body>
    
    <footer>
        
        
        
    </footer>
    
</html>