///////////////////////////////////////////////
//onClick Events for TEXT
//////////////////////////////////////////////
var textSize = 3;
var str = document.getElementById("pInfo").textContent;
document.getElementById("pInfo").innerHTML = str.fontsize(textSize);

function fontBold(){
    document.getElementById("pInfo").style.fontWeight = "900";
}

function fontItalic(){
    document.getElementById("pInfo").style.fontStyle = "italic";
}

function fontNormal(){
    document.getElementById("pInfo").removeAttribute("style");
}

function fontPlus(){
    if (textSize < 5){
        textSize ++;
    }
    document.getElementById("pInfo").innerHTML = str.fontsize(textSize);
}

function fontMinus(){
        if (textSize > 1){
        textSize--;
    }
    document.getElementById("pInfo").innerHTML = str.fontsize(textSize);
}